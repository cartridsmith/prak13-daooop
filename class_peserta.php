<?php
require_once "class_DAO.php";
class Peserta extends DAO
{
    public function __construct()
    {
        parent::__construct("peserta");
    }
    public function simpan($data){
       $sql = "INSERT INTO ".$this->tableName.
       " (id,nomor,email,namalengkap,kegiatan_id,jenis_id)".
        " VALUES (default,?,?,?,?,?)";
        $ps = $this->koneksi->prepare($sql);
        $ps->execute($data);
        return $ps->rowCount();
    }

    public function ubah($data){
        $sql = "UPDATE ".$this->tableName.
        " SET nomor=?,email=?,namalengkap=?,kegiatan_id=?,jenis_id=? "." WHERE id=?";

        $ps = $this->koneksi->prepare($sql);
        $ps->execute($data);
        return $ps->rowCount();
    }
}
?>